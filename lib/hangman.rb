class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
  end


  def setup
    length = @referee.pick_secret_word
    @guesser.register_secret_length(length)
    @board = Array.new(length)
  end

  def update_board(matches, guess)
    matches.each {|i| @board[i] = guess}
  end

  def take_turn
    guess = @guesser.guess(@board)
    matches = @referee.check_guess(guess)
    self.update_board(matches, guess)
    @guesser.handle_response(guess, matches, @board)
  end

  def won?
    !@board.include?(nil)
  end

  def play
    until self.won?
      self.take_turn
    end
    print "congratulations!"
  end
end

class HumanPlayer

  attr_accessor :secret_length

  def register_secret_length(length)
    @secret_length = length
  end

  def pick_secret_word
    puts "How many letters in your word?"

    length = gets.chomp
  end

  def guess(board)
    puts "guess a letter"
    guess = gets.chomp
  end

  def check_guess(guess)
    print guess
    puts "Please tell us the index (or indices) of the guess (e.g. 1,3). If the word does not contain this letter, please press enter."
    matches = gets.chomp
    match_ary = []
    matches.each_char do |chr|
      if chr.to_i.to_s == chr
        match_ary << chr.to_i
      end
    end

    match_ary
  end

  def handle_response(letter, indices, board)
    display = board.map do |char|
      if char == nil
        "_"
      else
        char
      end
    end

    print "Secret word: #{display.join(" ")}"
  end
end

class ComputerPlayer
  def initialize(dictionary = "dictionary.txt")
    if dictionary[-3..-1] == "txt"
      @dictionary = []
      File.foreach(dictionary) {|line| dictionary << line.chomp}
    else
      @dictionary = dictionary
    end

    @secret_word = ""
  end

  attr_reader :dictionary, :secret_word, :secret_length

  def pick_secret_word
    @secret_word = @dictionary.sample

    @secret_word.length
  end

  def register_secret_length(length)
    @secret_length = length
    @dictionary.delete_if {|word| word.length != length}
  end

  def guess(board)
    all_letters = @dictionary.join.split("")
    all_letters.delete_if {|ltr| board.include?(ltr)}

    all_letters.max_by {|ltr| all_letters.count(ltr)}
  end

  def check_guess(letter)
    matches = []
    @secret_word.each_char.with_index {|chr, i| matches << i if chr == letter}

    matches
  end

  def handle_response(letter, indices, board = nil)
    unless indices.empty?
      @dictionary.delete_if {|word| !word.include?(letter)}
      @dictionary.each do |word|
        indices.each do |i|
          dictionary.delete(word) if word[i] != letter
        end
        word.each_char.with_index {|chr, idx| dictionary.delete(word) if chr == letter && !indices.include?(idx)}
      end
    else
      @dictionary.delete_if {|word| word.include?(letter)}
    end
  end

  def candidate_words
    @dictionary
  end




end
